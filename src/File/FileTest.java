package File;

import java.io.File;

/*
* File类的判断方法
* */
public class FileTest {
    /**
     * File类的方法：
     * - public boolean exists():判断是否存在
     * - public boolean isDirectory():判断是否是目录
     * - public boolean isFile():判断是否是文件
     * - public boolean canRead():判断是否可读
     * - public boolean canWrite():判断是否可写
     * - public boolean canExecute():判断是否可执行
     * - public boolean isHidden():判断是否隐藏
     * @param args
     */

    public static void main(String[] args) {
        //file就是一个路径
        File file = new File("/Users/xiaokang/Desktop/study/file");

        //判断是否是目录
        System.out.println(file.isDirectory());
        //判断是否是文件
        System.out.println(file.isFile());
        //判断是否存在
        System.out.println(file.exists());
        //判断是否可读
        System.out.println(file.canRead());
        //判断是否可写
        System.out.println(file.canWrite());
        //判断是否可执行
        System.out.println(file.canExecute());
        //判断是否是隐藏文件
        System.out.println(file.isHidden());

    }
}
