package File;

import java.io.File;
import java.io.IOException;

/*
* File类创建文件和文件夹
* */
public class FileTest1 {
    /**
     *
     * - public boolean createNewFile():创建文件 如果存在这样的文件，就不创建了
     * - public boolean mkdir():创建文件夹 如果存在这样的文件夹，就不创建了
     * - public boolean mkdirs():创建文件夹,如果父文件夹不存在，会帮你创建出来
     * @param args
     */
    public static void main(String[] args) {
        File file = new File("/Users/xiaokang/Desktop/study/file/test");

        //创建文件夹
        //mkdirs既可以创建一级文件夹，又可以创建多级文件夹（mkdirs包含了mkdir）,mkdir不行。
        boolean mkdirs = file.mkdirs();
        System.out.println(mkdirs);

        //创建一个文件
        try {
            File file1 = new File("/Users/xiaokang/Desktop/study/file/test/1.txt");

            file1.createNewFile();//谁调用异常，
            //IOException：创建，删除，修改文件等等，都可能出现IOException。
        } catch (IOException ioException) {
            ioException.printStackTrace();
        }

        //如果文件夹或者文件存在，就无法创建，返回false。

        //判断文件夹是否存在，不存在才会创建。
        if(!file.exists()){
            file.mkdirs();
        }
    }
}
