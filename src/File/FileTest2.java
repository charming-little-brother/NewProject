package File;

import java.io.File;

/*
* File类重命名和删除
* */
public class FileTest2 {
    /**
     * - public boolean renameTo(File dest):把文件重命名为指定的文件路径
     * - public boolean delete():删除文件或者文件夹
     * - delete()只能删除空文件夹。
     * @param args
     */

    public static void main(String[] args) {
        File file = new File("/Users/xiaokang/Desktop/study/file/test/1.txt");
        File file1 = new File("/Users/xiaokang/Desktop/study/file/test/xx");

        //重命名
        boolean b = file.renameTo(new File("/Users/xiaokang/Desktop/study/file/test/2.txt"));
        System.out.println(b);

        //删除
        boolean bd = file1.delete();
        System.out.println(bd);

    }
}
