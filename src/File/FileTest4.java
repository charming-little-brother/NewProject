package File;

/*
* File类获取文件和文件夹的方法
* */

import java.io.File;
import java.util.Arrays;

public class FileTest4 {
    /**
     * - public String[] list():获取指定目录下的所有文件或者文件夹的名称数组
     * - public File[] listFiles():获取指定目录下的所有文件或者文件夹的File数组
     * @param args
     */

    public static void main(String[] args) {
        File file = new File("/Users/xiaokang/Desktop/study/file/test");

        File[] files = file.listFiles();

        System.out.println(Arrays.toString(files));
    }
}
