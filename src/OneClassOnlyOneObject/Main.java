package OneClassOnlyOneObject;

/*
* Main方法
* */
public class Main {

    public static void main(String[] args) {
        /**
         * 执行Test类
         */
        //声明对象1
//        Test instance1 = Test.getInstance();
//        //声明对象2
//        Test instance2 = Test.getInstance();
//        //查看对象1和对象2是否相同
//        System.out.println(instance1 == instance2);//true


        /**
         * 执行Demo类
         */
        for (int i = 0; i < 100; i++) {
            Demo instance = Demo.getInstance();
            System.out.println(instance);
        }
    }
}
