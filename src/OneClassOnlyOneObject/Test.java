package OneClassOnlyOneObject;

/*
* 单例模式
*
* 这个类，只能有一个对象。
* */
public class Test {

    /**
     * 实现方式一
     *
     * 1、要有一个private的构造方法。
     * 2、在类的内部去new一个private的对象。
     * 3、在类中提供一个静态的方法，获取第二步中生成的对象。
     */
    //饿汉式
//    private Test() {}
//    private static final Test test = new Test();
//    public static Test getInstance() {
//        return test;
//    }

    /**
     * 实现方式二
     *
     * 1、要有一个private的构造方法。
     * 2、在类的内部定义一个类的变量。
     * 3、在类中提供一个静态的方法，第一次访问时，生成一个对象，以后的访问，都去读这个对象。
     */
    //懒汉式
    private Test() {}
    private static Test test = null;
    public static synchronized Test getInstance() {
        if(test == null) {
            test = new Test();
        }
        return test;
    }
}
