package OneClassOnlyOneObject;

import java.util.ArrayList;
import java.util.List;
import java.util.Random;

/*
* 多例
*
* 生成一定数量的对象
* */
public class Demo {

    //防止外部new对象
    private Demo() {}

    //一次性生成10个对象
    private static List<Demo> demoList = new ArrayList<>();

    static {
        for (int i = 0; i < 10; i++) {
            Demo demo = new Demo();
            demoList.add(demo);
        }
    }
    
    //每次外部调用时，给外部一个对象
    public static Demo getInstance() {
        Random random = new Random();
        int i = random.nextInt(10);
        return demoList.get(i);
    }
}
