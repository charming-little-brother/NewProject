package Collection.Test;
/*
* 可变参数
* */
public class ChangeParam {
    /**
     * 可变长度参数，可以传0～n个值
     * 可变长度参数，实际上是一个数组，当使用时，如果传n个参数，就是n个长度的数组。
     * 一个方法只能有一个可变长度参数。
     * 如果方法里有多个参数，可变长度参数要放到最后面。
     * @param args
     */
    public static void main(String[] args) {
        //输出带参数的方法
        System.out.println(getTotal(1,2));
        System.out.println(getTotal(1,2,3));
        System.out.println(getTotal(1,2,3,4));
    }

    //遍历数组长度
    public static Integer getTotal(Integer...arrays){
        int total = 0;
        for (Integer i:arrays) {
            total += i;
        }
        return total;
    }
}
