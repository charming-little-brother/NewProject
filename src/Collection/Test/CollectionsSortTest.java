package Collection.Test;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;

/*
* Sort排序
* */
public class CollectionsSortTest {

    public static void main(String[] args) {
        //字符串排序
        ArrayList arrayList = new ArrayList();
        arrayList.add("www");
        arrayList.add("baidu");
        arrayList.add("com");
        System.out.println(arrayList);
        //排序(首字母排序)
        Collections.sort(arrayList);
        System.out.println(arrayList);

        ArrayList<Student> arrayList1 = new ArrayList<>();
        arrayList1.add(new Student("赵六",6));
        arrayList1.add(new Student("张三",3));
        arrayList1.add(new Student("王五",5));
        arrayList1.add(new Student("李四",4));
        arrayList1.add(new Student("孙七",4));

        Collections.sort(arrayList1, new Comparator<Student>() {
            @Override
            public int compare(Student o1, Student o2) {
                if(o1.getAge() == o2.getAge()){
                    return o1.getName().length() - o2.getName().length();
                }
                return o1.getAge() - o2.getAge();
            }
        });
        System.out.println(arrayList1);
    }
}
