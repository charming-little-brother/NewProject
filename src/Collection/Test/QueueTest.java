package Collection.Test;

import java.util.LinkedList;

/*
* 队列
* */
public class QueueTest {
    /**
     * 队列
     * 实现：链表（LinkedList）
     * 特点：先进先出
     *
     * LinkedList特点：查询慢，增删快。
     * @param args
     */
    public static void main(String[] args) {
        LinkedList<Integer> linkedList = new LinkedList<>();

        //在头部位置添加
        linkedList.addFirst(1);
        linkedList.addFirst(2);

        //在最后位置添加
        linkedList.addLast(3);

        linkedList.push(4);//等同于addFirst
        linkedList.add(5);//等同于addLast

        //取值
        Integer first = linkedList.getFirst();
        System.out.println("first" + first);

        Integer last = linkedList.getLast();
        System.out.println("last" + last);

        System.out.println(linkedList);

        //移除
        linkedList.removeFirst();
        linkedList.removeLast();

        System.out.println(linkedList);

        //遍历
        for (Integer i:linkedList) {
            System.out.println(i);
        }
    }
}
