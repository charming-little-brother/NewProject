package Collection.Test;

import java.util.HashSet;

/*
* hashSet
* */
public class HashSetTest {
    /**
     * HashSet特点：
     *            1，无序
     *            2，不存在重复的值。
     * @param args
     */
    public static void main(String[] args) {
        String str = "张三";
        System.out.println(str.hashCode());

        HashSet<Integer> hashSet = new HashSet<>();
        hashSet.add(2);
        hashSet.add(1);
        hashSet.add(3);
        hashSet.add(3);
        System.out.println(hashSet);

        //hashset中的元素，存储时，会先计算这个元素的hashcode值，计算完后，再根据数组大小取模，最终看放到什么位置。
        HashSet<Student> students = new HashSet<>();

//        Student student = new Student("张三", 3);
//        Student student1 = new Student("张三", 4);
//        Student student2 = new Student("张三", 5);
//        Student student3 = new Student("张三", 3);
//        System.out.println(student.hashCode());
//        System.out.println(student1.hashCode());
//        System.out.println(student2.hashCode());
//        System.out.println(student3.hashCode());

        students.add(new Student("张三",3));
        students.add(new Student("李四",4));
        students.add(new Student("王五",5));
        students.add(new Student("王五",6));
        students.add(new Student("王五",5));
        System.out.println(students);
    }
}
