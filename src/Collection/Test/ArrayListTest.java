package Collection.Test;

import java.util.ArrayList;
import java.util.Iterator;

/*
 * ArrayList
 * */
public class ArrayListTest {
    public static void main(String[] args) {
        /**
         * ArrayList特点:
         *              1，有序
         *              2，可以存重复的值
         *              3，有索引下标
         *              4.查询快，增删慢
         */
        /*
        System.out.println("Hello World");
        System.out.println("2022-03-10 07:37:14");
        System.out.println("测试");
        System.out.println(1);
        */

        //集合
        //生成无参的构造方法，默认容量为10的数组。
        ArrayList arrayList = new ArrayList();
        //生成容量为10000的有参的构造方法。
        ArrayList arrayList1 = new ArrayList(10000);

        //存数据
        //直接存储
        arrayList.add("张三");
        //根据索引下标存储
        arrayList.add(0,"李四");

        arrayList.add("张三");
        arrayList.add("王五");

        //取数据（只能获取索引内的）
        arrayList.get(0);

        //循环输出arrayList的数据
        for (int i = 0; i < arrayList.size(); i++) {
            System.out.println(arrayList.get(i));
        }

        //删除数据
        //根据对象删除
        arrayList.remove("张三");//只会删除第一个
        //根据下标删除
        arrayList.remove(1);

        //ArrayList的方法
        /**
         * size() 大小
         * clear() 清除
         * indexOf("参数") 返回参数在队列中第一次出现的索引位置，如果没有，则返回-1。
         * lastIndexOf("参数") 返回参数在队列中最后一次出现的索引位置，如果没有，则返回-1。
         * isEmpty() 判断当前队列是否为空队列，返回true或者false
         * toArray() 把队列转换成一个数组，数组大小为size的大小,如果类似于student的类，要重写equal方法。
         * contains("参数") 判断队列中是否包含"参数"，返回true或者false
         */

        System.out.println("队列大小:" + arrayList.size());
        //队列清除
//        arrayList.clear();
        System.out.println("第一次在队列中出现的位置:" + arrayList.indexOf("张三"));
        System.out.println("最后一次在队列中出现的位置:" + arrayList.lastIndexOf("张三"));

        System.out.println(arrayList.isEmpty());

        //迭代器
        //把队列里的偶数删除
        ArrayList arrayList2 = new ArrayList();
        arrayList2.add(1);
        arrayList2.add(2);
        arrayList2.add(2);
        arrayList2.add(4);
        arrayList2.add(5);

        Iterator<Integer> iterator = arrayList2.iterator();
        //判断迭代器iterator是否有下一个，true/false
        while (iterator.hasNext()){
            //如果有，就输出
            Integer next = iterator.next();
            //如果输出的是偶数，删除掉。
            if(next%2 == 0){
                iterator.remove();
            }
        }
        //最后，输出队列
        System.out.println(arrayList2);
    }
}
