package Collection.Test;

import java.util.LinkedHashSet;

/*
* LinkedHashSet
* */
public class LinkedHashSetTest {
    /**
     * hashSet = 数组 + 单链表（长度大于8的时候，会转化为红黑数）
     * LinkedHashSet：解决了hashSet无序的特点
     * LinkedHashSet在hashSet的基础上加了一个链表。
     * @param args
     */

    public static void main(String[] args) {
        LinkedHashSet<Integer> linkedHashSet = new LinkedHashSet<>();
        linkedHashSet.add(1);
        linkedHashSet.add(3);
        linkedHashSet.add(2);
        System.out.println(linkedHashSet);
    }
}
