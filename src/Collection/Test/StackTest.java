package Collection.Test;

import java.util.Iterator;
import java.util.Stack;

/*
* 栈
* */
public class StackTest {
    /**
     * 在栈中存数据（压栈，入栈）是以1，2，3，的顺序。
     *      取数据（出栈）以3，2，1，的顺序取。
     * 特点：先进后出
     * @param args
     */
    public static void main(String[] args) {
        Stack<Integer> stack = new Stack<>();
        //存入栈中
        stack.push(1);
        stack.push(2);
        stack.push(3);

        //看一眼栈中
        Integer peek = stack.peek();
        System.out.println("看一眼" + peek);

        //弹出栈中
        Integer pop = stack.pop();
        System.out.println("弹出的元素" + pop);

        //输出栈
        System.out.println(stack);

        //遍历栈
        Iterator<Integer> iterator = stack.iterator();
        while (iterator.hasNext()){
            System.out.println(iterator.next());
        }

    }
}
