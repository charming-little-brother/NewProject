package Collection.Test;

import java.util.Objects;

/*
* 重写toString，equals,hashCode
* */
public class Student {//implements Comparable{

    private String name;
    private Integer age;

    public Student() {

    }

    public Student(String name, Integer age) {
        this.name = name;
        this.age = age;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Integer getAge() {
        return age;
    }

    public void setAge(Integer age) {
        this.age = age;
    }

    @Override
    public String toString() {
        return "Student{" +
                "name='" + name + '\'' +
                ", age=" + age +
                '}';
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Student student = (Student) o;
        return Objects.equals(name, student.name) && Objects.equals(age, student.age);
    }

    @Override
    public int hashCode() {
        return Objects.hash(name, age);
    }


    /**
     * //先按照年龄的升序排
     *     //如果年龄相等，就按照姓名的长度排序
     *     @Override
     *     public int compareTo(Object o) {
     *         Student s = (Student) o;
     *
     *         if(this.age.equals(s.getAge())){
     *             return this.name.length() - s.getName().length();
     *         }
     *         //如果返回正数，升序。
     *         return this.age - s.getAge();
     *         //如果返回负数，降序。
     * //        return s.getAge() - this.age;
     *     }
     */
}
