package Collection.Test;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;

/*
 * Collections方法
 * */
public class CollectionsUtils {
    /**
     * shuffle  打乱集合的内容
     * reverse  对集合的内容进行反转
     * addAll  往指定的List中，添加多个元素
     * replaceAll  把指定List中的元素替换，可以一次替换多个
     * Sort  对指定队列进行排序，默认是升序（从小到大排序）
     * @param args
     */

    public static void main(String[] args) {
        ArrayList<Integer> arrayList = new ArrayList<>();
        arrayList.add(1);
        arrayList.add(2);
        arrayList.add(3);
        arrayList.add(4);
        //打乱顺序
        Collections.shuffle(arrayList);
        System.out.println(arrayList);

        //反转
        Collections.reverse(arrayList);
        System.out.println(arrayList);

        //添加
        Collections.addAll(arrayList,3,4,5,1);
        System.out.println(arrayList);

        //替代
        Collections.replaceAll(arrayList,1,100);
        System.out.println(arrayList);

        //int排序
        Collections.sort(arrayList);
        System.out.println(arrayList);
    }
}
