package Collection.Test;

import java.util.Arrays;
import java.util.List;
/*
* Arrays方法
* */

public class ArrayUtils {
    /**
     * toString  打印数组所有的元素
     * sort  把数组按照升序排序
     * equals  判断两个数组是否相等
     * fill  填充数组里面的元素
     * asList  把数组转换成一个List集合
     * @param args
     */

    public static void main(String[] args) {
        Integer[] ints = {1,2,3,6,4,8};
        //输出的是数组地址
        System.out.println(ints);
        //输出数组的
        System.out.println("toString方法：" + Arrays.toString(ints));

        //sort方法
        Arrays.sort(ints);
        System.out.println("sort方法：" + Arrays.toString(ints));

        //equals判断两个数组是否相等
        Integer[] ints1 = {1,4,2,6,7};
        Integer[] ints2 = {1,4,2,6,7};
        boolean equals = Arrays.equals(ints1,ints2);
        System.out.println(equals);

        //asList把数组转换成一个List集合
        Integer[] ints3 = {5,4,2,6,7};
        List<Integer> integers = Arrays.asList(ints3);
        System.out.println(integers);

        //fill填充数组里面的元素
        Integer[] ints4 = {5,4,2,6,7,5,1};
        //IllegalArgumentException非法数据异常
        //Arrays.fill(ints4,1,0,999);
        Arrays.fill(ints4,1,3,999);
        System.out.println(Arrays.toString(ints4));
    }
}
