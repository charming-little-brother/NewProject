package reflection;

/*
* 1、所有的类，都有一个自己的Class对象。
* 2、获取Class对象的三种方法，对应三个阶段
* 3、无论用什么方法获取的Class对象，任何一个类，只有一个对象。
* */
public class Demo {

    public static void main(String[] args) throws ClassNotFoundException {
        //生成一个class类的三种方法

        //1.
        Class clz1 = Class.forName("reflection.Student");

        //2.
        Class clz2 = Student.class;

        //3.
        Student student = new Student();
        Class clz3 = student.getClass();

        System.out.println(clz1==clz2);//true
        System.out.println(clz3==clz2);//true

        System.out.println(int.class==Integer.class);//false
    }
}
