package reflection;

import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.util.Properties;

/*
* 根据info.properties里的地址和方法去创建对象和执行方法。
*
* */
public class Main {

    public static void main(String[] args) {
        //读取文件，获取路径和方法名
        InputStream is = null;

        //本身的类Properties
        Properties properties = new Properties();

        try {
            is = Main.class.getClassLoader().getResourceAsStream("info.properties");

            properties.load(is);

            String className = properties.getProperty("className");
            String methodName = properties.getProperty("methodName");

            //根据className创建对象
            Class<?> aClass = Class.forName(className);
            Student student = (Student)aClass.newInstance();
            
            //获取方法
            Method method = aClass.getMethod(methodName);
            Object invoke = method.invoke(student);
            System.out.println(invoke);

        } catch (IOException ioException) {
            ioException.printStackTrace();
        } catch (ClassNotFoundException e) {
            e.printStackTrace();
        } catch (InstantiationException e) {
            e.printStackTrace();
        } catch (IllegalAccessException e) {
            e.printStackTrace();
        } catch (InvocationTargetException e) {
            e.printStackTrace();
        } catch (NoSuchMethodException e) {
            e.printStackTrace();
        } finally {
            if (is != null) {
                try {
                    is.close();
                } catch (IOException ioException) {
                    ioException.printStackTrace();
                }
            }
        }
    }
}
