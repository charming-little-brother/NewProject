package reflection;

/*
* Student类
* */
public class Student {

    private String name;
    private Integer age;

    public String aa;
    String bb;
    protected String cc;

    public Student() {
    }

    public Student(String name, Integer age, String aa, String bb, String cc) {
        this.name = name;
        this.age = age;
        this.aa = aa;
        this.bb = bb;
        this.cc = cc;
    }

    public Student(String name, Integer age) {
        this.name = name;
        this.age = age;
    }

    private Student(String name) {
    }

    private Student(String name, Integer age, String xxx) {

    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Integer getAge() {
        return age;
    }

    public void setAge(Integer age) {
        this.age = age;
    }

    public String getAa() {
        return aa;
    }

    public void setAa(String aa) {
        this.aa = aa;
    }

    public String getBb() {
        return bb;
    }

    public void setBb(String bb) {
        this.bb = bb;
    }

    public String getCc() {
        return cc;
    }

    public void setCc(String cc) {
        this.cc = cc;
    }

    @Override
    public String toString() {
        return "Student{" +
                "name='" + name + '\'' +
                ", age=" + age +
                ", aa='" + aa + '\'' +
                ", bb='" + bb + '\'' +
                ", cc='" + cc + '\'' +
                '}';
    }

    private void sleep() {
        System.out.println("睡觉");
    }

    public void read(String s1) {
        System.out.println("读" + s1);
    }

    public void read(String s1,String s2) {
        System.out.println("读" + s1 + " " + s2);
    }

    public String read() {
        return "自习室";
    }
}
