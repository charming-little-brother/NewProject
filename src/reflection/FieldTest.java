package reflection;

import java.lang.reflect.Field;
import java.util.Arrays;

/*
* 反射
*
* 获取一个类的Field对象(成员变量)
* */
public class FieldTest {

    public static void main(String[] args) throws NoSuchFieldException, IllegalAccessException {
        Class clz = Student.class;

        /**
         * getFields:只能获取public的成员变量的数组。
         * getDeclaredFields:获取所有修饰符类型的变量数组。
         * getField(String):用来获取一个public成员变量的Field对象。
         * getDeclaredField(String):可以获取所有修饰符类型的Field对象。
         */

        //通过类描述，来获取这个类里面所有成员变量的对象。
        Field[] fields = clz.getFields();
        System.out.println(Arrays.toString(fields));

        Field[] declaredField = clz.getDeclaredFields();
        System.out.println(Arrays.toString(declaredField));

        //通过public成员变量的名字，获取这个成员变量的Field对象
        Field aa = clz.getField("aa");
        System.out.println(aa);

        //通过private成员变量的名字，获取这个成员变量的Field对象
        Field name = clz.getDeclaredField("name");
        System.out.println(name);


        Student student = new Student();
        //给student对象的public的变量aa赋值10
        aa.set(student,"10");

        //给student对象的private的变量name赋值
        name.setAccessible(true);//赋值前必须暴力反射
        name.set(student,"zhansan");
        System.out.println(student);

        //从student对象中，获取name字段的值
        Object o = name.get(student);
        System.out.println(o);
    }
}
