package reflection;

import java.lang.reflect.Constructor;
import java.lang.reflect.InvocationTargetException;

/*
 * 反射
 *
 * 获取一个类的Construct对象(构造方法)
 * */
public class ConstructTest {

    public static void main(String[] args) throws NoSuchMethodException, InvocationTargetException, InstantiationException, IllegalAccessException {
        Class clz = Student.class;

        /**
         * 获取所有的public构造函数
         */
        Constructor[] constructors = clz.getConstructors();
        for (Constructor constructor:constructors) {
            System.out.println(constructor);
        }


        System.out.println("分割线=======================================");
        /**
         * 获取所有的构造函数（public,private,default,private）
         */
        Constructor[] declaredConstructors = clz.getDeclaredConstructors();
        for (Constructor declaredConstructor:declaredConstructors) {
            System.out.println(declaredConstructor);
        }


        System.out.println("分割线=======================================");
        /**
         * 获取无参的构造函数
         */
        Constructor constructor = clz.getConstructor();
        System.out.println(constructor);
        //new一无参的对象
        Object o = constructor.newInstance();
        System.out.println(o);


        System.out.println("分割线=======================================");
        /**
         * 获取有参的构造函数（String,Integer）
         */
        Constructor constructor1 = clz.getConstructor(String.class, Integer.class);
        System.out.println(constructor1);
        //new一个有参的对象
        Object zhansan = constructor1.newInstance("zhansan", 18);
        System.out.println(zhansan);


        System.out.println("分割线=======================================");
        /**
         * 获取有参的private构造函数（String,Integer,String）
         */
        Constructor constructor2 = clz.getDeclaredConstructor(String.class, Integer.class, String.class);
        System.out.println(constructor2);
    }
}
