package reflection;

/*
* 反射
*
* class的其它方法
* */
public class ClassOtherMethod {

    public static void main(String[] args) {
        Class clz = Student.class;

        //获取class类的类加载器
        ClassLoader classLoader = clz.getClassLoader();
        System.out.println(classLoader);

        //获取class
        Class<? extends Class> aClass = clz.getClass();
        System.out.println(aClass);

        //获取这个class所在的包对象
        Package aPackage = clz.getPackage();
        System.out.println(aPackage);
    }
}
