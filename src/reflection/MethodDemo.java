package reflection;

import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;

/*
 * 反射
 *
 * 获取一个类的Method(成员方法)
 * */
public class MethodDemo {

    public static void main(String[] args) throws NoSuchMethodException, InvocationTargetException, IllegalAccessException {
        Class<Student> clz = Student.class;

        /**
         * 获取所有的public方法（包括父类Object的方法）
         */
        Method[] methods = clz.getMethods();
        for (Method method:methods) {
            System.out.println(method);
        }


        System.out.println("分割线=======================================");
        /**
         * 获取所有方法,但不包括父类Object的方法
         */
        Method[] declaredMethods = clz.getDeclaredMethods();
        for (Method declaredMethod:declaredMethods) {
            System.out.println(declaredMethod);
        }


        System.out.println("分割线=======================================");
        /**
         * 获取read(String s1)方法
         */
        Method method = clz.getMethod("read", String.class);
        System.out.println(method);


        System.out.println("分割线=======================================");
        /**
         * 获取read(String s1,String s2)方法
         */
        Method method1 = clz.getMethod("read", String.class,String.class);
        System.out.println(method1);

        
        System.out.println("分割线=======================================");
        /**
         * 相当于student.read("射雕英雄传");
         */
        Student student = new Student();
        //无返回值的方法
//        Object invoke = method1.invoke(student,"射雕英雄传");
        //有返回值的方法
        Method returnRead = clz.getMethod("read");
        Object invoke = returnRead.invoke(student);
        System.out.println(invoke);
    }
}
