package Factory;

/*
* 普通的工厂模式
*
* 非静态
* */
public class NormalFactoryTest {

    public Person getInstance() {
        return new Person();
    }
}
