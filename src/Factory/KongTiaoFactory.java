package Factory;

/*
* 空调工厂的实现类
*
* */
public class KongTiaoFactory {

    public static Ikongtiao getInstance(String type) {
        if(type.equals("bianpin")) {
            return new BianpinImpl();
        } else if (type.equals("jieneng")) {
            return new JienengImpl();
        }
        return null;
    }
}
