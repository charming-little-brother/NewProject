package Factory;

public class Main {

    public static void main(String[] args) {
        //NormalFactoryTest类
//        NormalFactoryTest normalFactoryTest = new NormalFactoryTest();
//        Person instance = normalFactoryTest.getInstance();

        //StaticFactoryTest
        //可以直接调用，不需要new
//        StaticFactoryTest.getInstance();

        //空调工厂模式
        Ikongtiao instance1 = KongTiaoFactory.getInstance("bianpin");
        System.out.println(instance1);

    }
}
