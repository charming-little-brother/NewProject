package Factory;

/*
* 静态的工厂模式
*
* */
public class StaticFactoryTest {

    public static Person getInstance() {
        return new Person();
    }
}
