package com.banyuan;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;

//模拟斗地主发牌，并排序。
public class PokerTest {

    public static void main(String[] args) {
        //输出一副牌
        String[] color = {"♥️","♠️","♣️","♦️"};
        String[] number = {"A","2","3","4","5","6","7","8","9","10","J","Q","K"};

        List<Poker> arrayList = new ArrayList();
        arrayList.add(new Poker("大王",53));
        arrayList.add(new Poker("小王",52));

        int i = 0;
        for (String col : color) {
            for (String num : number) {
                arrayList.add(new Poker(col + num,i));
                i++;
            }
        }

        //把集合里的数据顺序全部打乱
        Collections.shuffle(arrayList);
//        System.out.println(arrayList);

        //new3个玩家,每人17张。
        List<Poker> player1 = new ArrayList();
        List<Poker> player2 = new ArrayList();
        List<Poker> player3 = new ArrayList();
        //余下的3张牌
        List<Poker> threePoker = new ArrayList();

        //发牌
        for (int j = 0; j < arrayList.size(); j++) {
            if (j >= 51) {
                threePoker.add(arrayList.get(j));
            }
            if (j%3 == 0) {
                player1.add(arrayList.get(j));
            }
            if (j%3 == 1) {
                player2.add(arrayList.get(j));
            }
            if (j%3 == 2) {
                player3.add(arrayList.get(j));
            }
        }
        //输出
        System.out.println("玩家一:" + sort(player1));
        System.out.println("玩家二:" + sort(player2));
        System.out.println("玩家三:" + sort(player3));
        System.out.println("余下的3张牌:" + sort(threePoker));
    }

    //排序
    public static List<Poker> sort(List<Poker> pokerList){
        Collections.sort(pokerList, new Comparator<Poker>() {
            @Override
            public int compare(Poker o1, Poker o2) {
                return o1.getNumber() - o2.getNumber();
            }
        });
        return pokerList;
    }

}
