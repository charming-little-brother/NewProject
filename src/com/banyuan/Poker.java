package com.banyuan;


import java.util.Objects;

//扑克牌类
public class Poker {

    private String color;
    private Integer number;

    public String getColor() {
        return color;
    }

    public void setColor(String color) {
        this.color = color;
    }

    public Integer getNumber() {
        return number;
    }

    public void setNumber(Integer number) {
        this.number = number;
    }

    public Poker() {
    }

    public Poker(String color, Integer number) {
        this.color = color;
        this.number = number;
    }

    @Override
    public String toString() {
        return "Poker{" +
                "color='" + color + '\'' +
                ", number=" + number +
                '}';
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Poker poker = (Poker) o;
        return Objects.equals(color, poker.color) && Objects.equals(number, poker.number);
    }

    @Override
    public int hashCode() {
        return Objects.hash(color, number);
    }
}
