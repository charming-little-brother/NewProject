package Annotation;

/*
* 注解
* */

import Enumerate.Sex;

import java.lang.annotation.*;

import static java.lang.annotation.ElementType.*;

@Target({TYPE,FIELD,METHOD,PARAMETER,CONSTRUCTOR})//自定义注解，表示可以注解在类，字段，方法，范围和构造方法上。
@Retention(RetentionPolicy.RUNTIME)//注解代码在哪个阶段（SOURCE,CLASS,RUNTIME）。
@Documented//注解是否参与到生成文档中。
@Inherited//注解是否允许继承。
public @interface MyAnno {
    int age() default 0;//基础数据类型，如果没有设置值，默认使用成default到值。

    String name();//String类型

    Sex sex();//枚举

//    MyAnno1 myann();//可以是注释

    String[] address();//上面4种类型到数组

    String className();
    String methodName();

}
