package Annotation;

import Enumerate.Sex;

import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.util.Arrays;

/*
* 注解
*
* 获取类(Class)，方法(Method)，字段(Field)对象
* 通过上面对象，调用getAnnotation(注解名.class)，获取标注的注解对象
* 通过注解的对象，获取注解里面的值。
* */
@MyAnno(name = "张三",sex = Sex.FEMALE,address = {"南京","上海"},className = "reflection.Student",methodName = "eat")
public class Test {

    public static void main(String[] args) {
        Class<Test> testClass = Test.class;

        //获取Test上面标注的MyAnno注解
        MyAnno annotation = testClass.getAnnotation(MyAnno.class);

        //判断注解是否为空
        if(annotation != null) {
            String className = annotation.className();
            String methodName = annotation.methodName();

//            System.out.println(className);
//            System.out.println(methodName);
            
            try {
                //通过类的全路径地址，获取这个类的class对象
                Class<?> aClass = Class.forName(className);
                
                //通过class对象，就能new一个实例
                Object o = aClass.newInstance();

                //通过class对象，获取method对象
                Method method = aClass.getMethod(methodName);
                method.invoke(o);

            } catch (ClassNotFoundException e) {
                e.printStackTrace();
            } catch (InstantiationException e) {
                e.printStackTrace();
            } catch (IllegalAccessException e) {
                e.printStackTrace();
            } catch (InvocationTargetException e) {
                e.printStackTrace();
            } catch (NoSuchMethodException e) {
                e.printStackTrace();
            }
        }
    }
}
