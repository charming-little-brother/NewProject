package Enumerate;

/*
* 枚举
* */
public class Test {

    public static void main(String[] args) {

        Student student = new Student();

        //用枚举中定义的性别
        student.setSex(Sex.MALE);
        System.out.println(student.getSex());
    }
}
