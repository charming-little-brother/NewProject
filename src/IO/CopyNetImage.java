package IO;

/*
* 获取百度图片，保存到本地。
* */

import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.net.MalformedURLException;
import java.net.URL;

public class CopyNetImage {

    public static void main(String[] args) throws MalformedURLException {

        //获取时间戳
        Long long1 = System.currentTimeMillis();

        InputStream is = null;
        OutputStream os = null;

        try {

            //定义一个URL
            URL url = new URL("https://www.baidu.com/img/PCtm_d9c8750bed0b3c7d089fa7d55720d6cf.png");

            //打开这个网络的地址，或者这个地址的流
            is = url.openStream();

            //拷贝的地址
            os = new FileOutputStream("/Users/xiaokang/Desktop/study/file/test/baidu.png");

            IOUtils.copy(is,os);
        } catch (MalformedURLException e) {
            e.printStackTrace();
        } catch (IOException ioException) {
            ioException.printStackTrace();
        } finally {
            //关闭
            IOUtils.CloseQuitely(os);
            IOUtils.CloseQuitely(is);
        }

        //计算获取图片到本地的时间
        Long long2 = System.currentTimeMillis();
        System.out.println(long2 - long1);
    }
}
