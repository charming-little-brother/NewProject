package IO;

/*
 * 字符流
 * 读
 * */

import java.io.*;

public class ReaderTest {

    /**
     * 字符流是基于字节流
     * @param args
     */

    public static void main(String[] args) {

        //字节流的写
        InputStream is = null;

        //字符流的写
        InputStreamReader isr = null;

        try {
            //字节流获取读的地址
            is = new FileInputStream("/Users/xiaokang/Desktop/study/file/test/2.txt");

            //用UTF-8编码把字符读入字符流
            isr = new InputStreamReader(is,"UTF-8");

            //每一次要读取的字符流的长度，直到读取完毕。
            char[] chars = new char[10];

            int len;
            while ((len = isr.read(chars)) > 0){
                //输出长度
                System.out.println(len);
                //输出截取的字节流的内容
                System.out.println(new String(chars,0,len));
            }

        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        } finally {
            //关闭
            IOUtils.CloseQuitely(isr);
            IOUtils.CloseQuitely(is);
        }
    }
}
