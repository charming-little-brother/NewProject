package IO;

/*
* 缓冲流
* 写
* */

import java.io.*;

public class BufferWriterTest {

    /**
     * 缓冲流是基于字符流，
     * 而字符流是基于字节流。
     * @param args
     */

    public static void main(String[] args) {

        //字节流的写
        OutputStream os = null;
        //字符流的写
        OutputStreamWriter osw = null;
        //缓冲流的写
        BufferedWriter bw = null;

        try {
            //字节流获取写的地址
            os = new FileOutputStream("/Users/xiaokang/Desktop/study/file/test/2.txt",true);
            //用UTF-8编码把字符写入字符流
            osw = new OutputStreamWriter(os,"UTF-8");
            //字符流赋值给缓冲流
            bw = new BufferedWriter(osw);

            bw.write("我是谁，谁是我，取完钱啊，addadd");
            //换行，或者叫新建一行
            bw.newLine();
            //强制输出残余数据，清空缓冲区
            bw.flush();
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        } finally {
            IOUtils.CloseQuitely(bw);
            IOUtils.CloseQuitely(osw);
            IOUtils.CloseQuitely(os);
        }
    }
}
