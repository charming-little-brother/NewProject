package IO;

import java.io.Serializable;//专门用来做序列化到
import java.util.Objects;

/*
* 序列化：把Java对象存储到硬盘。
* 反序列化：把键盘上的文件，恢复成对象，为了防止有人修改，会将serialVersionUID做比对。
* 一个类，要能够序列化，必须实现serialVersionUID接口
* */

public class Student implements Serializable {

    private static final long serialVersionUID = -6849794480754667710L;
    private String name;
    private Integer age;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Integer getAge() {
        return age;
    }

    public void setAge(Integer age) {
        this.age = age;
    }

    public Student(){

    }

    public Student(String name, Integer age) {
        this.name = name;
        this.age = age;
    }

    @Override
    public String toString() {
        return "Student{" +
                "name='" + name + '\'' +
                ", age=" + age +
                '}';
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Student student = (Student) o;
        return Objects.equals(name, student.name) && Objects.equals(age, student.age);
    }

    @Override
    public int hashCode() {
        return Objects.hash(name, age);
    }
}
