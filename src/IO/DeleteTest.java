package IO;

import java.io.File;

/*
* 删除一个文件夹（文件中可能包含文件或者文件夹）
*
* 如果一个文件夹下有文件，是无法删除的，必须先删除这个文件夹下的所有文件。
*
* 只要用程序删除，就会直接从硬盘删除，不会经过回收站和废纸篓。
* */
public class DeleteTest {

    public static void main(String[] args) {
        delete(new File("/Users/xiaokang/Desktop/study/file1"));
    }

    public static void delete(File fileDir) {

        //1.先获取这个文件下的所有文件和文件夹
        File[] files = fileDir.listFiles();

        for(File file:files) {
            //2.如果是文件就删除
            if (file.isFile()) {
                file.delete();
                //3，如果不是文件，是文件夹，就调用delete方法
            } else {
                delete(file);
            }
        }
        //4，删除自己本身的文件夹
        fileDir.delete();
    }
}
