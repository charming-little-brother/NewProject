package IO;

import java.io.*;
import java.util.Date;

/*
* 序列化
* 写
* */

public class ObjectOutputStreamTest {

    public static void main(String[] args) {

        OutputStream os = null;
        ObjectOutputStream objectOutputStream = null;

        try {
            //定义对象
            String s = "abc";
            Date date = new Date();
            Student student = new Student();

            //生成存储对象的流
            os = new FileOutputStream("/Users/xiaokang/Desktop/study/file/test/4.txt",true);
            objectOutputStream = new ObjectOutputStream(os);

            //把对象写入到硬盘
            objectOutputStream.writeObject(s);
            objectOutputStream.writeObject(date);
            objectOutputStream.writeObject(student);

            //当读取的时候，obj等于null，将不会执行。
            objectOutputStream.writeObject(null);

        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (IOException ioException) {
            ioException.printStackTrace();
        } finally {
            IOUtils.CloseQuitely(objectOutputStream);
            IOUtils.CloseQuitely(os);
        }

    }
}
