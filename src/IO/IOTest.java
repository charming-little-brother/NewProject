package IO;

import java.nio.charset.StandardCharsets;

/*
* IO流
*  -Input(Read)
*  -Output(Writer)
* */
public class IOTest {

    public static void main(String[] args) {
        String s = "ab你好";//在苹果电脑上，一个英文字母占1个字节，一个汉字占3个字节。

        //一个字符串，怎么去转化成byte数组
        byte[] bytes = s.getBytes();
        System.out.println(bytes.length);

        //byte数组转换成字符串
        String ss2 = new String(bytes);
    }
}
