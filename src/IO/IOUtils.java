package IO;

/*
* IO(字节流)的封装
* */

import java.io.Closeable;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;

public class IOUtils {

    /*
    * IOException要抛出，由调用的人来普获
    * io,os都要用户自己关闭
    * */

    /**
     * 拷贝方法
     * @param is
     * @param os
     * @throws IOException
     */
    //如果客户没有填写缓冲区的大小
    public static void copy(InputStream is,OutputStream os) throws IOException{

        //已经定义了缓冲区的大小
        Integer bufferSize = 1024*1024;

        copy(is,os,bufferSize);
    }

    /**
     *
     * @param is
     * @param os
     * @param bufferSize
     * @throws IOException
     */
    //读，写，缓冲区读大小
    public static void copy(InputStream is,OutputStream os,Integer bufferSize) throws IOException {

        if(is == null || os == null) {
            throw new IllegalArgumentException("参数异常");
        }

        if(bufferSize <= 0) {
            throw new IllegalArgumentException("参数异常，缓冲区大小不能小于0");
        }
        //缓冲区大小
        byte[] bytes = new byte[bufferSize];

        int len;
        //把从byte[]读取的字节赋值给len
        while ((len = is.read(bytes)) > 0) {
            os.write(bytes,0,len);
        }
    }


    /**
     * 关闭流
     * @param closeable
     */
    //InputStream和OutputStream都继承类Closeable这个类，
    public static void CloseQuitely(Closeable closeable) {

        if(closeable != null){
            try {
                closeable.close();
            } catch (IOException ioException) {
                ioException.printStackTrace();
            }
        }
    }

    //InputStream的封装
//    public static void CloseQuitely(InputStream is){
//
//        if(is != null){
//            try {
//                is.close();
//            } catch (IOException ioException) {
//                ioException.printStackTrace();
//            }
//        }
//
//    }
//
//    //OutputStream的封装
//    public static void CloseQuitely(OutputStream os){
//
//        if (os != null) {
//            try {
//                os.close();
//            } catch (IOException ioException) {
//                ioException.printStackTrace();
//            }
//        }
//    }
}
