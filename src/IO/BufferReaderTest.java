package IO;

/*
 * 缓冲流
 * 读
 * */

import java.io.*;

public class BufferReaderTest {

    /**
     * 缓冲流是基于字符流，
     * 而字符流是基于字节流。
     * @param args
     */

    public static void main(String[] args) {

        //字节流的读
        InputStream is = null;
        //字符流的读
        InputStreamReader isr = null;
        //缓冲流的读
        BufferedReader br = null;

        try {
            //字节流获取读的地址
            is = new FileInputStream("/Users/xiaokang/Desktop/study/file/test/2.txt");

            //用UTF-8编码把字符读入字符流
            isr = new InputStreamReader(is,"UTF-8");

            //字符流赋值给缓冲流
            br = new BufferedReader(isr);

            String s ;

            //readLine一读就读一行
            while ((s = br.readLine()) != null) {
                System.out.println(s);
            }

        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        } finally {
            IOUtils.CloseQuitely(br);
            IOUtils.CloseQuitely(isr);
            IOUtils.CloseQuitely(is);
        }
    }
}
