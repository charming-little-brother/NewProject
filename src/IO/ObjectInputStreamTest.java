package IO;

import java.io.*;

/*
 * 反序列化
 * 读
 * */

public class ObjectInputStreamTest {

    public static void main(String[] args) {
        InputStream is = null;
        ObjectInputStream ois = null;

        try {
            is = new FileInputStream("/Users/xiaokang/Desktop/study/file/test/4.txt");
            ois = new ObjectInputStream(is);


            Object obj;
            while ((obj = ois.readObject()) != null) {
                System.out.println(obj);
            }
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (IOException ioException) {
            ioException.printStackTrace();
        } catch (ClassNotFoundException e) {
            e.printStackTrace();
        } finally {
            IOUtils.CloseQuitely(ois);
            IOUtils.CloseQuitely(is);
        }
    }

}
