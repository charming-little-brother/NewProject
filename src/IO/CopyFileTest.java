package IO;

/*
 * 复制一个文件
 *
 * 方法二（IOUtils封装的方法）
 * */

import java.io.*;

public class CopyFileTest {

    public static void main(String[] args) {

        InputStream is = null;
        OutputStream os = null;

        try {
            //读
            is = new FileInputStream("/Users/xiaokang/Desktop/study/file/test/2.txt");
            //写
            os = new FileOutputStream("/Users/xiaokang/Desktop/study/file/test/4.txt");

            //调用IOUtils类封装的方法
            IOUtils.copy(is,os);
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (IOException ioException) {
            ioException.printStackTrace();
        } finally {
            //调用IOUtils类封装的方法
            //关闭
            IOUtils.CloseQuitely(os);
            IOUtils.CloseQuitely(is);
        }

    }
}
