package IO.HomeWork;

import org.apache.commons.io.IOUtils;

import java.io.*;
/*
* 声明对象，写入到本地文件中
* */
public class UserTest {

    public static void main(String[] args) {

        User user1 = new User("张三","123456");
        User user2 = new User("李四","789012");
        User user3 = new User("王五","126bgf");

        OutputStream os = null;
        OutputStreamWriter osr = null;
        BufferedWriter bw = null;

        try {
            os = new FileOutputStream("/Users/xiaokang/Desktop/study/file/test/2.txt",true);
            osr = new OutputStreamWriter(os,"UTF-8");
            bw = new BufferedWriter(osr);

//            bw.write(user1.getUsername(),user1.getPassword());
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (UnsupportedEncodingException e) {
            e.printStackTrace();
        } finally {
            IOUtils.closeQuietly(bw);
            IOUtils.closeQuietly(osr);
            IOUtils.closeQuietly(os);
        }


    }
}
