package IO.HomeWork;

import org.apache.commons.io.IOUtils;

import java.io.*;
import java.text.NumberFormat;

/*
* 复制一个视频, 提示用户百分比完成度
* */
public class CopyVideoTest {

    public static void main(String[] args) {
        InputStream is = null;
        OutputStream os = null;

        try {
            is = new FileInputStream("/Users/xiaokang/Desktop/study/file/test/jdk-8u321-macosx-x64.dmg");

            //文件大小
            File file = new File("/Users/xiaokang/Desktop/study/file/test/jdk-8u321-macosx-x64.dmg");
            long length = file.length();

            os = new FileOutputStream("/Users/xiaokang/Desktop/study/file/test/jdk.dmg");

            byte[] bytes = new byte[100 * 1024];
            int len;
            while ((len = is.read(bytes)) > 0) {
                //流里面还剩多少字节
                int available = is.available();

                System.out.println(getPercent(length - available,length,3));

                os.write(bytes,0,len);
            }
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (IOException ioException) {
            ioException.printStackTrace();
        } finally {
            IOUtils.closeQuietly(os);
            IOUtils.closeQuietly(is);
        }
    }

    /**
     * 把两个数相除，展示百分比。
     * @param d1
     * @param d2
     * @param digit
     * @return
     */
    public static String getPercent(double d1,double d2,Integer digit) {
        //定义一个NumberFormat格式
        NumberFormat percentInstance = NumberFormat.getPercentInstance();
        //小数点后面保留digit位。
        percentInstance.setMinimumFractionDigits(digit);
        //输出一个百分比字符串
        String format = percentInstance.format(d1 / d2);
        return format;
    }
}
