package IO.HomeWork;

import org.apache.commons.io.IOUtils;

import java.io.*;
import java.util.Scanner;

/*
* 模拟用户注册, 将用户录入的数据保存到本地文件中,以"="号连接
*
* 方法一
* */
public class UserLoginTest {

    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        System.out.println("请输入账号名和密码");
        String userName = scanner.nextLine();
        String passWord = scanner.nextLine();


        OutputStream os = null;
        Writer writer = null;
        BufferedWriter bw = null;

        try {
            //字节流
            os = new FileOutputStream("/Users/xiaokang/Desktop/study/file/test/2.txt",true);
            //字符流
            writer = new OutputStreamWriter(os,"UTF-8");
            //缓冲流
            bw = new BufferedWriter(writer);

            bw.write(userName);
            bw.write("=");
            bw.write(passWord);
            //换行，等同于"\n"。
            bw.newLine();
            //确保缓冲区
            bw.flush();

        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        } finally {
            IOUtils.closeQuietly(bw);
            IOUtils.closeQuietly(writer);
            IOUtils.closeQuietly(os);
        }
    }
}
