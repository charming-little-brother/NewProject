package IO.HomeWork;

import org.apache.commons.io.IOUtils;

import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.OutputStream;
import java.util.Scanner;

/*
 * 模拟用户注册, 将用户录入的数据保存到本地文件中,以"="号连接
 *
 * 方法三(IOUtils方法)
 *
 * 异常：谁调用谁处理。
 * 流，谁定义谁关。
 * */
public class UserLoginTest2 {

    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        System.out.println("请输入账号名和密码");
        String userName = scanner.nextLine();
        String passWord = scanner.nextLine();

        OutputStream os = null;
        try {
            os = new FileOutputStream("/Users/xiaokang/Desktop/study/file/test/2.txt",true);
            IOUtils.write(userName + "" + passWord + "\n",os,"UTF-8");
        } catch (IOException e) {
            e.printStackTrace();
        } finally {
            IOUtils.closeQuietly(os);
        }
    }
}
