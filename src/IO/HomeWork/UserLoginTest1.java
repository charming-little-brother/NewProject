package IO.HomeWork;

import org.apache.commons.io.FileUtils;

import java.io.File;
import java.io.IOException;
import java.util.Scanner;

/*
 * 模拟用户注册, 将用户录入的数据保存到本地文件中,以"="号连接
 *
 * 方法二(FileUtils方法)
 * */
public class UserLoginTest1 {

    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        System.out.println("请输入账号名和密码");
        String userName = scanner.nextLine();
        String passWord = scanner.nextLine();

        //把用户名和密码存储到文件中
        File file = new File("/Users/xiaokang/Desktop/study/file/test/2.txt");
        try {
            //往什么文件写，写什么内容，用什么编码，是否在文件内容后面追加。
            FileUtils.writeStringToFile(file,userName + "=" + passWord + "\n","UTF-8",true);
        } catch (IOException ioException) {
            ioException.printStackTrace();
        }
    }
}
