package IO.HomeWork;

import org.apache.commons.io.IOUtils;

import java.io.*;

/*
* 将一个音乐文件拆分成多个文件,放到一个文件夹中,这个文件夹以音乐文件的名称命名
*
* 将被拆分的文件合并到一起，组成一个新的文件，让音乐可以继续播放。
* */
public class FileSplitMergeTest {

    public static void main(String[] args) {
//        File file = new File("/Users/xiaokang/Desktop/study/file/test/jdk-8u321-macosx-x64.dmg");
//        split(file,10*1024*1024);

        merge("/Users/xiaokang/Desktop/study/file/test/jdk-8u321-macosx-x64/","jdk.dmg");
    }

    /**
     * 将一个音乐文件拆分成多个文件，放到一个文件夹中，这个文件夹以音乐文件名称命名。
     * @param file 要拆分的文件
     * @param size 按照多大进行进行拆分
     */
    public static void split(File file, Integer size) {
        //获取文件的名称
        String name = file.getName();
        System.out.println(name);
        //获取文件路径
        File absoluteFile = file.getAbsoluteFile();
        System.out.println(absoluteFile);
        //去除后缀，截取文件名
        int indexOf = name.indexOf(".");
        String substring = name.substring(0, indexOf);
        System.out.println(substring);

        //获取文件所在的目录
        String parent = file.getParent();
        System.out.println(parent);

        //通过文件名创建一个文件夹
        new File(parent,substring).mkdirs();

        //读取流
        InputStream is = null;
        try {
            is = new FileInputStream(absoluteFile);

            byte[] bytes = new byte[size];
            int len;
            int i = 1;
            while ((len = is.read(bytes)) > 0 ) {
                System.out.println(len);
                System.out.println(parent + "/" + substring + "/" + i);
                //写
                OutputStream os = new FileOutputStream(parent + "/" + substring + "/" + i);
                os.write(bytes,0,len);
//                System.out.println(len);
                i++;
                os.close();
            }
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (IOException ioException) {
            ioException.printStackTrace();
        } finally {
            IOUtils.closeQuietly(is);
        }
    }

    /**
     * 合并文件
     * @param path 要合并的文件所在的文件夹
     * @param name 合并后的文件名。
     */
    public static void merge(String path,String name) {
        OutputStream os = null;
        try {
            //生成一个输出流，就是要合并的文件
            os = new FileOutputStream(path + name);
            //获取文件路径
            File file = new File(path);
            //计算文件夹下的文件个数
            File[] files = file.listFiles();
            int size = files.length;//20
            System.out.println(size);

            //有隐藏文件，所有需要-1
            for (int i = 1; i <= size-1; i++) {
                System.out.println(path + i);
                InputStream is = new FileInputStream(path + i);

                byte[] bytes = new byte[10*1024*1024];
                int len;
                while ((len = is.read(bytes)) > 0) {
                    os.write(bytes,0,len);
                }
                is.close();
            }
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (IOException ioException) {
            ioException.printStackTrace();
        } finally {
            IOUtils.closeQuietly(os);
        }
    }
}
