package IO.HomeWork;

import org.apache.commons.io.IOUtils;

import java.io.*;
import java.util.HashMap;
import java.util.Scanner;

/*
* 读取文件"身份证所在地.csv“，用户输入身份证前6位，显示当前用户的地址。
* */
public class IdCardTest {
    public static void main(String[] args) {
        //1，读取文件
        InputStream is = null;
        InputStreamReader isr = null;
        BufferedReader br = null;

        try {
            is = new FileInputStream("/Users/xiaokang/Desktop/study/file/test/身份证所在地.csv");
            isr = new InputStreamReader(is,"UTF-8");
            br = new BufferedReader(isr);

            String s;
            HashMap<String, String> hashMap = new HashMap<>();
            while ((s = br.readLine()) != null) {
                //以"，"分隔。
                String[] split = s.split(",");
                //2，把身份证的前六位做key，把地区做value。
                hashMap.put(split[0],split[1]);
            }
//            System.out.println(hashMap);

            Scanner scanner = new Scanner(System.in);
            System.out.println("请输入身份证的前六位");
            String s1 = scanner.nextLine();
            System.out.println(hashMap.get(s1));

        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        } finally {
            IOUtils.closeQuietly(br);
            IOUtils.closeQuietly(isr);
            IOUtils.closeQuietly(is);
        }
    }
}
