package IO.HomeWork;

import org.apache.commons.io.FileUtils;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;

/*
* 模拟用户登录
* */

public class LoginMain {

    private static File file = new File("/Users/xiaokang/Desktop/study/file/test/5.txt");

    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        System.out.println("欢迎登录，请输入用户名：");
        String username = scanner.nextLine();

        System.out.println("欢迎" + username + "登录：请输入密码：");
        String password = scanner.nextLine();

        try {
            //登录成功
            if(login(username,password)) {
                System.out.println("欢迎" + username + "登录成功");
            //用户名在，密码错误。
            } else if (isexisUserName(username)) {
                System.out.println("密码错误，输入Y重置密码：");
                String yesrono = scanner.nextLine();
                if(yesrono.equals("Y")) {
                    System.out.println("输入新密码");
                    String newPw = scanner.nextLine();
                    System.out.println("重复输入新密码：");
                    String reNewPw = scanner.nextLine();

                    if(!newPw.equals(reNewPw)) {
                        System.out.println("两次输入的密码不相等");
                    }else {
                        //修改密码
                        updatePassword(username,newPw);
                        System.out.println("修改密码成功");
                    }
                }else {
                    System.out.println("输入的不是Y");
                }
            //用户名不存在，注册
            } else {
                System.out.println("用户名不存在，请按Y注册");
                String yorn = scanner.nextLine();
                if(yorn.equals("Y")) {
                    System.out.println("请输入注册的用户名：");
                    String registerUsername = scanner.nextLine();

                    System.out.println("请输入注册的密码：");
                    String registerPassword = scanner.nextLine();

                    System.out.println("注册成功");

                    while (isexisUserName(registerUsername)) {
                        System.out.println("用户名重复，请重新注册");
                        System.out.println("请输入注册的用户名：");
                        registerUsername = scanner.nextLine();

                        System.out.println("请输入注册的密码：");
                        registerPassword = scanner.nextLine();
                    }
                    saveUser(registerUsername,registerPassword);
                }else {
                    System.out.println("推出程序");
                }
            }
        } catch (IOException ioException) {
            ioException.printStackTrace();
        }
    }

    //获取文件中所有的用户，放在一个集合中
    public static List<User> getAllUser() throws IOException {
        List<String> strings = FileUtils.readLines(file, "UTF-8");
        List<User> userList = new ArrayList<>();
        for (String s:strings) {
            //split底层，用"，"把字符串分割开，分割开的每一个字符串再存到一个数组中
            String[] split = s.split(",");
            User user = new User(split[0], split[1]);
            userList.add(user);
        }
        return userList;
    }
    
    //1，拿到输入的用户名和密码，去文件中做比对,判断是否登录成功
    public static boolean login(String username,String password) throws IOException {
        boolean flag = false;
        List<User> allUser = getAllUser();
        for (User user:allUser) {
            if(user.getUsername().equals(username) && user.getPassword().equals(password)) {
                flag = true;
                break;
            }
        }
        return flag;
    }
    //2，判断用户是否存在的方法
    public static boolean isexisUserName(String username) throws IOException {
        boolean flag = false;
        List<User> allUser = getAllUser();
        for (User user:allUser) {
            if (user.getUsername().equals((username))) {
                flag = true;
                break;
            }
        }
        return flag;
    }
    //3，注册，把输入的用户名和密码存储到文件中
    public static void saveUser(String username,String password) throws IOException {
        FileUtils.writeStringToFile(file,username + "," + password + "\n","UTF-8",true);
    }
    //4，修改密码。
    public static  void updatePassword(String username,String newPassword) throws IOException {
        //获取所有的用户信息
        List<User> allUser = getAllUser();
        //清空user.txt文档
        FileUtils.writeStringToFile(file,"","UTF-8");
        for (User user:allUser) {
            if (user.getUsername().equals(username)) {
                user.setPassword(newPassword);
            }
            saveUser(user.getUsername(),user.getPassword());
        }
    }
}
