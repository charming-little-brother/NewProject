package IO.HomeWork;

import org.apache.commons.io.FileUtils;

import java.io.File;
import java.io.IOException;

/*
* 拷贝一个文件夹
* */
public class CopyFileTest1 {

    public static void main(String[] args) {
        //源文件
        File file = new File("/Users/xiaokang/Desktop/study/file/test");
        //目标文件
        File file1 = new File("/Users/xiaokang/Desktop/study/file/test1");

        try {
            FileUtils.copyDirectory(file,file1);
        } catch (IOException ioException) {
            ioException.printStackTrace();
        }
    }
}
