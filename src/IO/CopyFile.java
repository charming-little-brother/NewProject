package IO;

/*
* 复制一个文件
*
* 方法一
* */

import java.io.*;

public class CopyFile {

    public static void main(String[] args) {

        InputStream is = null;
        OutputStream os = null;

        try {
            //读
            is = new FileInputStream("/Users/xiaokang/Desktop/study/file/test/2.txt");

            //写
            os = new FileOutputStream("/Users/xiaokang/Desktop/study/file/test/3.txt");

            //读取字节大小
            byte[] bytes = new byte[1024];

            //读取的数量
            int len;
            while ((len = is.read(bytes)) > 0) {
                os.write(bytes,0,len);
            }

            //抛出权限异常
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (IOException ioException) {
            ioException.printStackTrace();
        } finally {
            //封装类的方法
            IOUtils.CloseQuitely(is);
            IOUtils.CloseQuitely(os);
        }
    }
}
