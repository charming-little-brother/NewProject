package IO;

import java.io.*;
import java.util.HashMap;

/*
* 读取info.properties文件中对象的值。
* 基本写法
* */
public class ReadProperties {

    public static void main(String[] args) {
        InputStream is = null;
        Reader isr = null;
        BufferedReader br = null;

        try {
            //读取src下的文件，生成一个InputStream
            is = ReadProperties.class.getClassLoader().getResourceAsStream("info.properties");
            isr = new InputStreamReader(is);
            br = new BufferedReader(isr);

            //读
            String s;
            HashMap<String, String> hashMap = new HashMap<>();
            while ((s = br.readLine()) != null) {
//                System.out.println(s);

                String[] split = s.split("=");
                hashMap.put(split[0],split[1]);
            }
            System.out.println(hashMap);
        } catch (IOException ioException) {
            ioException.printStackTrace();
        } finally {
            IOUtils.CloseQuitely(br);
            IOUtils.CloseQuitely(isr);
            IOUtils.CloseQuitely(is);
        }
    }
}
