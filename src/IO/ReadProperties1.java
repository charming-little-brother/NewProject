package IO;

import org.apache.commons.io.IOUtils;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.Reader;
import java.util.Properties;

/*
* 未来读取info.properties文件，都使用这种方法
* */
public class ReadProperties1 {

    public static void main(String[] args) {
        InputStream is = null;

        try {
            is = ReadProperties.class.getClassLoader().getResourceAsStream("info.properties");

            Properties properties = new Properties();
            //读取is这个输入流，然后解析这个文件
            properties.load(is);

            //获取properties文件中的键，返回就是后面读值
            String name = properties.getProperty("name");
            String age = properties.getProperty("age");
            String dir = properties.getProperty("dir");
            /*
            * 如果在info.properties文件中没有赋值，可以在获取时赋值
            * String age = properties.getProperty("age","100");
            * */
            System.out.println(name);
            System.out.println(age);
            System.out.println(dir);


        } catch (IOException ioException) {
            ioException.printStackTrace();
        } finally {
            IOUtils.closeQuietly(is);
        }

    }
}
