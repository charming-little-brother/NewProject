package IO;

/*
* 字符流
* 写
* */

import java.io.*;

public class WriterTest {

    /**
     * 字符流是基于字节流
     * @param args
     */

    public static void main(String[] args) {

        //字节流的写
        OutputStream os = null;

        //字符流的写
        Writer writer = null;

        try {
            //字节流获取写的地址
            os = new FileOutputStream("/Users/xiaokang/Desktop/study/file/test/2.txt",true);

            //用UTF-8编码把字符写入字符流
            writer = new OutputStreamWriter(os,"UTF-8");

            //写入
            writer.write("我是中国人");
            writer.write("我是中国人");
            writer.write("我是中国人");
            writer.write("我是中国人");

            //一次性把写入的东西存到文件中
            writer.flush();

        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        } finally {
            //关闭
            IOUtils.CloseQuitely(writer);
            IOUtils.CloseQuitely(os);
        }
    }
}
