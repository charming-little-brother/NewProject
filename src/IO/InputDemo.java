package IO;

import com.sun.xml.internal.fastinfoset.util.PrefixArray;

import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;

/*
* 字节流
* InputStream
* 读
* */
public class InputDemo {

    public static void main(String[] args) {
        //读取文件，展示文件内容。
        InputStream is = null;
        try {
            is = new FileInputStream("/Users/xiaokang/Desktop/study/file/test/2.txt");

            //每一次要读取的字符流的长度，直到读取完毕。
            byte[] bytes = new byte[50];
            //把从文件中读取的数据，放到一个byte[]数组中。
//            int read = is.read(bytes);//读取了多少字节，就返回读取的数量。

//            System.out.println(read);

            //判断读取了多少字节
            int read;
            while ((read = is.read(bytes)) > 0) {
                //byte数组转换成字符串
                System.out.println(new String(bytes));
            }

            //权限异常
        } catch (FileNotFoundException e) {
            e.printStackTrace();

            //输入输出异常,读和写异常。
        } catch (IOException ioException) {
            ioException.printStackTrace();
        } finally {
//            if(is != null) {
//                try {
//                    is.close();
//                } catch (IOException ioException) {
//                    //什么都不做
//                }
//            }

            //调用封装
            IOUtils.CloseQuitely(is);
        }
    }
}
