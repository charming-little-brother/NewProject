package IO;

import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.OutputStream;
import java.nio.charset.StandardCharsets;

/*
* 字节流
* OutputStream
* 写
* */
public class OutputDemo {
    /**
     * 注意⚠️:流一定要关闭。
     * @param args
     */

    public static void main(String[] args) {
        //java提供了一个类，专门往硬盘文件中写数据。
        OutputStream os = null;
        try {
            //创建文件，异常是为了文件创建不成功准备的。
            //FileOutputStream，构造方法中，第二个参数如果是true，表示往文件后面添加，如果不写默认false（添加的内容会覆盖文件中所有的内容）。
            os = new FileOutputStream("/Users/xiaokang/Desktop/study/file/test/2.txt",true);//文件输出流（文件中写东西用的）

            String s = "hellp地球";

            //写入流
            os.write(s.getBytes());

            //FileNotFoundException 最多的时候发生无权限
        } catch (FileNotFoundException e) {
            e.printStackTrace();

            //输入输出异常,读和写异常。
        } catch (IOException ioException) {
            ioException.printStackTrace();
        }finally {
//            if (os != null) {
//                try {
//                    //关闭流（必须）
//                    os.close();
//                } catch (IOException ioException) {
//                    //什么都不做，静悄悄地关闭closeQuitely()。
//                }
//            }

            //调用封装
            IOUtils.CloseQuitely(os);
        }
    }
}
