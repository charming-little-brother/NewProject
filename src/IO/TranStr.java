package IO;

/*
* 输入一行简体字符串，转换成繁体。
* */

import java.io.*;
import java.util.HashMap;
import java.util.Map;
import java.util.Scanner;

public class TranStr {

    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        System.out.println("请输入简体字:");
        String simpStr = scanner.nextLine();

        /*
        * 1.读取文件
        * */
        InputStream is = null;
        InputStreamReader isr = null;
        BufferedReader br = null;

        try {
            //读取（缓冲流）
            is = new FileInputStream("/Users/xiaokang/Downloads/jf_map_utf8.txt");
            isr = new InputStreamReader(is,"UTF-8");
            br = new BufferedReader(isr);

            String s;

            //new一个存储拆分字符串的集合
            Map<String, String> map = new HashMap<>();

            while ((s = br.readLine()) != null) {
                //打印输出所有字符串
//                System.out.println(s);

                /*
                * 2.获取所有读值后（样式：龙:龍:40857），split分割字符串，放入Map集合中
                * */
                String[] split = s.split(":");
                map.put(split[0],split[1]);
            }


            StringBuffer multStr = new StringBuffer();

            for (int i = 0; i < simpStr.length(); i++) {
                //把输入的字符串拆分成一个一个的char
                char c = simpStr.charAt(i);
                //把上面拆分的char转化成String,在根据Map的key获取value
                String mult = map.get(String.valueOf(c));

                //判断文本中是否有输入简体字所对应的繁体字
                if (mult == null) {
                    multStr.append(c);
                } else {
                    multStr.append(mult);
                }
            }

            //转化成字符串
            System.out.println(multStr.toString());

        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        } finally {
            //关闭
            IOUtils.CloseQuitely(br);
            IOUtils.CloseQuitely(isr);
            IOUtils.CloseQuitely(is);
        }

    }
}
