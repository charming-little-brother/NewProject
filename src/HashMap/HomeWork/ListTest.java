package HashMap.HomeWork;

import java.util.*;

public class ListTest {

    public static void main(String[] args) {

        /*
        *  创建俩个List集合,添加一些数据，求它们的并集，差集和交集。
        * */

        List<Integer> list1 = new ArrayList();
//        list.add(1);
//        list.add(2);
//        list.add(3);
//        list.add(4);
        Collections.addAll(list1,1,2,3,4);
        System.out.println(list1);

        List<Integer> list2 = new ArrayList();
        Collections.addAll(list2,3,4,5,6);
        System.out.println(list2);

        /*
        * 交集
        * */
        //方法一
        List<Integer> jiaojiList = new ArrayList<>();
        for (Integer i:list1) {
            if (list2.contains(i)){
                jiaojiList.add(i);
            }
        }

        System.out.println("交集:" + jiaojiList);

        //方法二
//        list1.retainAll(list2);//从list1中移除list2
//        System.out.println(list1);

        /*
        * 并集
        * */
        //方法一
        Set<Integer> bingjiSet = new HashSet<>();
        for (Integer i:list1) {
            bingjiSet.add(i);
        }
        for (Integer i:list2) {
            bingjiSet.add(i);
        }
        System.out.println("并集:" + bingjiSet);

        //方法二
//        list1.removeAll(list2);//把list1中有的，list2中也有的部分排除掉，结果[1，2]
//        list1.addAll(list2);//[1,2][3,4,5,6]
//        System.out.println(list1);

        /*
        * 差集
        * */
        //方法一
        List<Integer> chajiList = new ArrayList<>();
        for (Integer i:list1) {
            if (!list2.contains(i)) {
                chajiList.add(i);
            }
        }
        System.out.println("差集:" + chajiList);

        //方法二
//        list1.removeAll(list2);
//        System.out.println(list1);
    }
}
