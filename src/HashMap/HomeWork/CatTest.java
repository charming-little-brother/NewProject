package HashMap.HomeWork;

import java.util.HashMap;
import java.util.Map;
import java.util.Set;

public class CatTest {

    public static void main(String[] args) {

        Cats c1 = new Cats("奥拓",100);
        Cats c2 = new Cats("宝马",200);
        Cats c3 = new Cats("奔驰",300);

        HashMap<Cats, Integer> m1 = new HashMap<>();
        m1.put(c1,10000);
        m1.put(c2,500000);
        m1.put(c3,2000000);

        System.out.println(m1);

        //遍历m1的键，打印name属性
        Set<Cats> cats = m1.keySet();
        for (Cats key :cats) {
            System.out.println("name:" + key.getName());
        }

        //通过合适的方法，求出m1中“宝马”的价格，并打印结果
        System.out.println("宝马的价格:" + m1.get(c2));

        //经过降速，所有汽车都降速到原来的80%，请打印降速后“宝马”的速度
        for (Map.Entry<Cats,Integer> entry:m1.entrySet()) {
//            System.out.println(entry.getKey());
            if(entry.getKey().getName().equals("宝马")){
                System.out.println("降速后宝马的速度" + entry.getKey().getSpeed() * 0.8);
            }
        }
    }
}


