package HashMap.HomeWork;

import java.io.Serializable;//序列化接口
import java.util.Objects;

public class Cats implements Serializable {

    private String name;

    private Integer speed;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Integer getSpeed() {
        return speed;
    }

    public void setSpeed(Integer speed) {
        this.speed = speed;
    }

    public Cats(){

    }

    public Cats(String name, Integer speed) {
        this.name = name;
        this.speed = speed;
    }

    @Override
    public String toString() {
        return "Cats{" +
                "name='" + name + '\'' +
                ", speed=" + speed +
                '}';
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Cats cats = (Cats) o;
        return Objects.equals(name, cats.name) && Objects.equals(speed, cats.speed);
    }

    @Override
    public int hashCode() {
        return Objects.hash(name, speed);
    }
}
