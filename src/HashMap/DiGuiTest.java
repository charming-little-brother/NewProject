package HashMap;
/*
* 递归
* */
public class DiGuiTest {
    /**
     * 递归就是自己调自己
     * @param args
     */

    public static void main(String[] args) {
        //输入n,计算5+4+3+2+1
        System.out.println(sum(5));

        //求5的阶乘
        System.out.println(jiecheng(5));
    }

    public static Integer sum(Integer n){
        if(n==1){
            return 1;
        }
        return n + sum(n - 1);
    }

    public static Integer jiecheng(Integer n){
        if(n==1){
            return 1;
        }
        return n * jiecheng(n - 1);
    }
}
