package HashMap;

import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;

/*
* hashmap的方法
* */
public class HashMapTest {
    /**
     * HashMap在存储值的时候，不允许键重复，值可以重复。
     * 特点
     *  1，如果存储重复的键，后面存储的内容，会覆盖前面的内容。
     *  2，存取顺序不一样。
     *  3，允许存储的键和值为null。
     *  方法：
     *  1，get 获取hashMap中指定键的值，如果没有返回null。
     *  2，size hashMap的大小
     *  3，clear 清空
     *  4，remove 删除键,返回对应键的值。
     *  5，isEmpty 判断hashMap是否为空
     *  6，containsKey 判断hashMap是否包含某个键
     *  7，containsValue 判断hashMap是否包含某个值
     *  8，keySet 把hashMap里的所有key存储到一个Set集合中
     *  9，entrySet 获取一个Map的Entry对象
     * @param args
     */

    public static void main(String[] args) {

        HashMap<String,Integer> hashMap= new HashMap();

        //存储
        hashMap.put("张三",3);
        hashMap.put("李四",4);
        hashMap.put("王五",5);
        hashMap.put("赵六",6);

        //获取hashMap中指定键的值，如果没有返回null。
        System.out.println(hashMap.get("张三"));

        //hashMap的大小
        System.out.println(hashMap.size());

        //清空
//        hashMap.clear();

        //删除键,返回对应键的值。
        System.out.println(hashMap.remove("张三"));

        //判断hashMap是否为空
        System.out.println(hashMap.isEmpty());

        //判断hashMap是否包含某个键
        //比较的是键的地址（两个对象是否==）
        System.out.println(hashMap.containsKey("赵六"));

        HashMap<Student,String> hashMap1 = new HashMap<>();
        hashMap1.put(new Student("我",7),"他");
        //在Student中重写了equals和hashCode，比较的是键的值。
        boolean b = hashMap1.containsKey(new Student("我",7));
        System.out.println(b);

        //判断hashMap是否包含某个值
        System.out.println(hashMap.containsValue(6));

        //把hashMap里的所有key存储到一个Set集合中
        System.out.println(hashMap.keySet());

        //题目1：获取一个集合中所有的key和value
        Set<String> keys = hashMap.keySet();
        for (String key:keys) {
            System.out.println("获取一个集合中所有的key和value：" + key + "=" + hashMap.get(key));
        }

        //题目2：获取一个Map的Entry对象
//        Set<Map.Entry<String, Integer>> entries = hashMap.entrySet();
//        for (Map.Entry<String, Integer> entry: entries) {
//            System.out.println("获取一个Map的Entry对象：" + entry.getKey() + entry.getValue());
//        }
        for (Map.Entry<String,Integer> entry:hashMap.entrySet()) {
            System.out.println("获取一个Map的Entry对象：" + entry.getKey() + entry.getValue());
        }
        System.out.println(hashMap);
    }
}
