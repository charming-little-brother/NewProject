package HashMap;

import java.util.Hashtable;
import java.util.LinkedHashMap;
import java.util.TreeMap;

/*
* HashTable
* TreeMap
* LinkedHashMap
* */
public class HashTableTest {
    /**
     * HashTable线程安全，效率低。
     * 不可以存储null键和null值。
     *
     * LinkedHashMap输出有序的Map集合
     * @param args
     */

    public static void main(String[] args) {
        Hashtable<String, Integer> hashtable = new Hashtable<>();
        hashtable.put("张三",3);
        //编译的时候不报错，运行的时候报NullPointerException
        hashtable.put(null,null);
        System.out.println(hashtable);

        TreeMap treeMap = new TreeMap();
        treeMap.put("李四",4);
        System.out.println(treeMap);

        LinkedHashMap linkedHashMap = new LinkedHashMap();
        linkedHashMap.put("www",5);
        linkedHashMap.put("baidu",6);
        linkedHashMap.put("com",7);
        System.out.println(linkedHashMap);
    }
}
