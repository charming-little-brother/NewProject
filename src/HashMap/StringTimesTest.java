package HashMap;

/*
* 从键盘输入一串数字，统计字符串中每个字符出现的次数，并获取最大值，打印最大键的值。
* */

import java.util.*;

public class StringTimesTest {

    public static void main(String[] args) {
        //1，获取输入的字符串
        Scanner scanner = new Scanner(System.in);
        System.out.println("请输入字符");
        String strs = scanner.nextLine();
//        System.out.println(strs);

        HashMap<Character, Integer> hashMap = new HashMap<>();

        //2，把字符串分解成一个个字符
        for (int i = 0; i < strs.length(); i++) {
            Character c = strs.charAt(i);

            //3、放入hashMap中，key是字母，value字母是个数。
            if(hashMap.containsKey(c)){
                //如果hashMap里有这个字符，拿出来+1，如果没有，就往hashMap里put一个字符，value赋值为1。
                Integer integer = hashMap.get(c);
                hashMap.put(c,integer + 1);
            }else {
                hashMap.put(c,1);
            }
        }
        System.out.println(hashMap);

        //获取最大值
        Collection<Integer> values = hashMap.values();
        int max = 0;
        for (Integer j:values) {
            if (j > max) {
                max = j;
            }
        }
        System.out.println(max);

        //打印最大键的值
        for (Map.Entry<Character,Integer> entry:hashMap.entrySet()) {
            if (entry.getValue().equals(max)) {
                System.out.println(entry.getKey() + "=" + entry.getValue());
            }
        }
    }
}
