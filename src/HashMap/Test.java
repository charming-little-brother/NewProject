package HashMap;

/*
* 冒泡排序
* */

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public class Test {
    public static void main(String[] args) {
        List<Integer> list = new ArrayList();
        list.add(3);
        list.add(2);
        list.add(9);
        list.add(7);
        list.add(1);
        list.add(3);
        System.out.println(list);

        for (int i = 0; i < list.size() - 1; i++) {
            for (int j = 0; j < list.size() - 1 - i; j++) {
                if(list.get(j) > list.get(j+1)){
                    Integer temp = list.get(j);
                    list.set(j,list.get(j+1));
                    list.set(j+1,temp);
                }
            }
        }
        System.out.println(list);

        System.out.println("=======================");

        List<Student> students = new ArrayList<>();
        students.add(new Student("李四",4));
        students.add(new Student("张三",3));
        students.add(new Student("王五",5));
        System.out.println(students);

        for (int s = 0; s < students.size() - 1; s++) {
            for (int st = 0; st < students.size() - 1 - s; st++) {
                if (students.get(st).getAge() > students.get(st + 1).getAge()){
                    Student temp = students.get(st);
                    students.set(st,students.get(st+1));
                    students.set(st+1,temp);
                }
            }
        }
        System.out.println(students);
    }
}
