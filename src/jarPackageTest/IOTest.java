package jarPackageTest;

import org.apache.commons.io.FileUtils;

import java.io.File;
import java.io.IOException;
import java.util.List;

/*
* 用.jar包里的封装方法读写文件
*
* 拷贝文件夹
* */

public class IOTest {

    public static void main(String[] args) {

        File file = new File("/Users/xiaokang/Desktop/study/file/test/2.txt");
        String s = "ddfaf;非法发放；啊发发哥哥；嘎嘎嘎嘎；阿甘我是vv啊吧";

        try {
            //往哪个文件写，写什么东西，用什么编码，是否追加。
//            FileUtils.writeStringToFile(file,s,"UTF-8",true);//写

            List<String> strings = FileUtils.readLines(file,"UTF-8");//读
            for (String str:strings) {
                System.out.println(str);
            }

            //拷贝文件夹
            File file1 = new File("/Users/xiaokang/Desktop/study/file");
            File file2 = new File("/Users/xiaokang/Desktop/study/file1");
            FileUtils.copyDirectory(file1,file2);

        } catch (IOException ioException) {
            ioException.printStackTrace();
        }
    }
}
