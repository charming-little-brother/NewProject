package ProcessAndThread;

import java.io.IOException;

/*
* Runtime
* 执行系统命令
* */
public class RuntimeTest {

    public static void main(String[] args) {
        Runtime runtime = Runtime.getRuntime();

        //执行参数中的命令
        try {
            runtime.exec("touch /Users/xiaokang/Desktop/study/file/test/6.txt");
        } catch (IOException ioException) {
            ioException.printStackTrace();
        }

    }
}
