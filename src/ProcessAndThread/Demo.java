package ProcessAndThread;

/*
* 线程的一些方法
* */
public class Demo {

    public static void main(String[] args) {

        Thread t1 = new Thread(new Runnable() {
            @Override
            public void run() {
                for (int i = 0; i < 50; i++) {
                    System.out.println("线程一运行中" + i);
                }
            }
        });

        Thread t2 = new Thread(new Runnable() {
            @Override
            public void run() {
                for (int i = 0; i < 200; i++) {
//                    try {
//                        //先让线程一跑完，再去跑线程二。
////                        t1.join();
//                        //先让线程一跑100毫米，再去跑线程二。
//                        t1.join(100);
//                    } catch (InterruptedException e) {
//                        e.printStackTrace();
//                    }
                    System.out.println("线程二运行中......" + i);

                    //让出当前线程的执行权
//                    Thread.yield();
                }
            }
        });

        //把线程设置成守护线程（线程一跑完后，就不跑线程二了。）
//        t2.setDaemon(true);

        //设置线程的优先级（不靠谱），优先级范围1～10。
        t1.setPriority(1);
        t2.setPriority(10);
        
        t1.start();
        t2.start();
    }
}
