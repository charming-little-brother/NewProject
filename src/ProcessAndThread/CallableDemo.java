package ProcessAndThread;

import java.util.concurrent.ExecutionException;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.Future;

/*
* Callable的实现
*
* 线程池
* */
public class CallableDemo {

    public static void main(String[] args) {
        CallableTest callableTest = new CallableTest();

        //创建一个大小为10的线程池
        ExecutorService executorService = Executors.newFixedThreadPool(10);

        //把一个线程对象，放到线程池中，进行
        Future<Boolean> submit = executorService.submit(callableTest);
        try {
            //获取线程执行完成后的返回值
            Boolean aBoolean = submit.get();
            System.out.println(aBoolean);
        } catch (InterruptedException e) {
            e.printStackTrace();
        } catch (ExecutionException e) {
            e.printStackTrace();
        }

        //销毁线程
        executorService.shutdown();
    }
}
