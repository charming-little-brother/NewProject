package ProcessAndThread;

/*
* ThreadLock是和线程绑定的，未来只要在这个线程内，set的值都不会改变
* */
public class ThreadLockTest {

    public static void main(String[] args) {
        ThreadLocal<String> threadLocal = new ThreadLocal<>();

        new Thread(new Runnable() {
            @Override
            public void run() {
                threadLocal.set("zhansan");

                try {
                    Thread.sleep(2000);
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }

                String s = threadLocal.get();
                System.out.println(Thread.currentThread().getName() + "的线程" + s);
            }
        },"zhansan").start();

        new Thread(new Runnable() {
            @Override
            public void run() {
                threadLocal.set("lisi");

                try {
                    Thread.sleep(2000);
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }

                String s = threadLocal.get();
                System.out.println(Thread.currentThread().getName() + "的线程" + s);
            }
        },"lisi").start();

        new Thread(new Runnable() {
            @Override
            public void run() {
                threadLocal.set("wangwu");

                try {
                    Thread.sleep(2000);
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }

                String s = threadLocal.get();
                System.out.println(Thread.currentThread().getName() + "的线程" + s);
            }
        },"wangwu").start();

    }
}
