package ProcessAndThread;

/*
* 卖票
* */

import java.util.concurrent.locks.Lock;
import java.util.concurrent.locks.ReentrantLock;

/**
 * Java里面，所有对象都可以充当一把锁
 *
 * 解决同步问题：
 * 1、使用同步方法，其实也有锁，用的是SellTickets.class对象。
 * 2、同步代码块，必须显示加锁，这把锁一般情况是全局变量。
 * 3、使用Java定义的锁Lock。
 */
public class SellTickets implements Runnable{

    private Integer tickets = 100;

//    @Override
//    public void run() {
//        sell();
//    }

    /**
     * 1、使用同步方法
     */
    /*
    * 只要有一个人进了这个方法，这个方法就会加一把锁，其他人再调用这个方法时，只能等着前面这个人执行完了，放开锁为止。
    * */
//    public synchronized void sell() {
//        while (true) {
//            if(tickets > 0) {
//                try {
//                    Thread.sleep(10);
//                } catch (InterruptedException e) {
//                    e.printStackTrace();
//                }
//
//                System.out.println(Thread.currentThread().getName() + "窗口，正在卖" + tickets + "票");
//                tickets--;
//            }
//        }
//    }

    /**
     * 2、同步代码块
     */
    //new一个对象
//    Object object = new Object();
//
//    public void sell(){
//        while (true) {
//            synchronized (object) {
//                if(tickets > 0) {
//                    try {
//                        Thread.sleep(10);
//                    } catch (InterruptedException e) {
//                        e.printStackTrace();
//                    }
//
//                    System.out.println(Thread.currentThread().getName() + "窗口，正在卖" + tickets + "票");
//                    tickets--;
//                }
//            }
//        }
//    }

    /**
     * 3、使用Java定义的锁Lock。
     */
    Lock lock = new ReentrantLock();
    @Override
    public void run() {
        while (true) {
            //关锁
            lock.lock();

            if(tickets > 0) {
                try {
                    Thread.sleep(10);

                    System.out.println(Thread.currentThread().getName() + "窗口，正在卖" + tickets + "票");
                    tickets--;

                } catch (InterruptedException e) {
                    e.printStackTrace();
                } finally {
                    //开锁
                    lock.unlock();
                }
            }
        }
    }
}
