package ProcessAndThread;

import java.util.Random;

/*
* 两个线程沟通的问题，生产者和消费者
* wait()
* notify()
* */
public class TranOfTest {

    //生产联系的锁
    private static String phone = "";
    //有没有消息
    private static boolean flag = false;
    //生产货物的数量
    private static Integer sum = 0;

    public static void main(String[] args) {
        //生产者
        new Thread(new Runnable() {
            @Override
            public void run() {
                while (true) {

                    //同步代码块
                    synchronized (phone) {

                        if(!flag) {
                            try {
                                //锁等待
                                phone.wait();
                            } catch (InterruptedException e) {
                                e.printStackTrace();
                            }
                        }

                        if(flag) {
                            System.out.println("接受订单，开始做");
                            try {
                                Thread.sleep(5000);
                            } catch (InterruptedException e) {
                                e.printStackTrace();
                            }

                            System.out.println("生产完成" + sum + "吨，通知消费者来取");
                            flag = false;
                            //锁通知
                            phone.notify();
                        }
                    }
                }
            }
        }).start();

        //消费者
        new Thread(new Runnable() {
            @Override
            public void run() {
                while (true) {
                    synchronized (phone) {
                        if(!flag) {
                            //生产0～9吨随机数
                            Random random = new Random();
                            int i = random.nextInt(10);

                            sum = i + 1;

                            flag = true;
                            System.out.println("通知生产商品:" + sum + "吨");
                            phone.notify();
                        }

                        if(flag) {
                            try {
                                phone.wait();
                            } catch (InterruptedException e) {
                                e.printStackTrace();
                            }
                        }

                        System.out.println("取货" + sum + "吨");

                        try {
                            Thread.sleep(2000);
                        } catch (InterruptedException e) {
                            e.printStackTrace();
                        }
                    }
                }
            }
        }).start();
    }
}
