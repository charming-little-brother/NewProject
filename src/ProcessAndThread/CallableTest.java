package ProcessAndThread;

import java.util.concurrent.Callable;

/*
* 多线程实现方式
* Callable
* */
public class CallableTest implements Callable<Boolean> {
    @Override
    public Boolean call() throws Exception {
        for (int i = 0; i < 20; i++) {
            System.out.println(i);
        }
        return true;
    }
}
