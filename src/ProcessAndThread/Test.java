package ProcessAndThread;

/*
* 实现多线程的方式
* */
public class Test {

    public static void main(String[] args) {
        /*
         * 创建多线程的方式一
         * */
        ThreadTest threadTest = new ThreadTest();
//        musicTest.run();还是在main线程中运行，没有启动多线程
        threadTest.start();
        for (int i = 0; i < 100; i++) {
            System.out.println("main线程工作1⃣️" + i);
        }

        /*
         * 创建多线程的方式二
         * */
        RunnableTest runnableTest = new RunnableTest();
        new Thread(runnableTest).start();
        for (int i = 0; i < 100; i++) {
            System.out.println("main线程工作2⃣️" + i);
        }

        /*
        * 创建多线程的方式三
        * */
        new Thread(){
            @Override
            public void run() {
                System.out.println("123qwe");
            }
        }.start();

        new Thread(new Runnable() {
            @Override
            public void run() {
                System.out.println("zxcasd");
            }
        }).start();
    }
}
