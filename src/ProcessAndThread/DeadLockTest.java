package ProcessAndThread;

/*
* 死锁
* */
public class DeadLockTest {

    public static void main(String[] args) {
        String s1 = "左筷子";
        String s2 = "右筷子";

        new Thread(new Runnable() {
            @Override
            public void run() {
                synchronized (s1) {
                    System.out.println(Thread.currentThread().getName() + "拥有" + s1);

                    //死锁开始
                    try {
                        Thread.sleep(20);
                    } catch (InterruptedException e) {
                        e.printStackTrace();
                    }

                    synchronized (s2) {
                        System.out.println(Thread.currentThread().getName() + "吃饭" + s2);
                    }
                }
            }
        },"张三丰").start();

        new Thread(new Runnable() {
            @Override
            public void run() {
                synchronized (s2) {
                    System.out.println(Thread.currentThread().getName() + "拥有" + s2);

                    //死锁开始
                    try {
                        Thread.sleep(20);
                    } catch (InterruptedException e) {
                        e.printStackTrace();
                    }

                    synchronized (s1) {
                        System.out.println(Thread.currentThread().getName() + "吃饭" + s1);
                    }
                }
            }
        },"达摩").start();
    }
}
