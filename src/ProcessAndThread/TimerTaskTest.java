package ProcessAndThread;

import java.util.Timer;
import java.util.TimerTask;

/*
* 任务类
* */
public class TimerTaskTest {

    public static void main(String[] args) {
        //1.先定义一个任务
        TimerTask timerTask = new TimerTask() {
            @Override
            public void run() {
                System.out.println("test timerTask");
            }
        };

        //2.定义执行上面任务的方式
        Timer timer = new Timer();
        timer.schedule(timerTask,4000);
    }
}
